from django.shortcuts import render
from datetime import datetime, date

# Enter your name here
mhs_name = 'Daffa Muhammad Rayhan' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 8, 28) #TODO Implement this, format (Year, Month, Date)
npm = 1706026954 # TODO Implement this
uni = 'Universitas Indonesia'
major = 'Information System'
hobby = 'Playing games and sports'
desc = 'I am someone who is humble, likes to help, outgoing, humorous, brave and creative. I can sometimes be an introvert and can also be an extrovert.' 


mhs_name2 = 'Drasseta Aliyyu Darmansyah'
birth_date2 = date(1999, 6, 1)
npm2 = 1706074953
major2 = 'Computer Science'
hobby2 = 'Drawing and reading'
desc2 = 'People call me Drass. I live in Depok. You can usually see me at the canteen eating satay or hanging around at the secretary square.'

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'uni': uni, 'major': major, 'hobby': hobby, 'desc': desc, 
                'name2': mhs_name2, 'age2': calculate_age(birth_date2.year), 'npm2': npm2, 'major2': major2, 'hobby2': hobby2, 'desc2': desc2}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
